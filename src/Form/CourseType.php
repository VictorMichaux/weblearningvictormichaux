<?php

namespace App\Form;

use App\Entity\Course;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du cours'
            ])
            ->add('smallDescription', TextType::class, [
                'label' => 'Petite description'
            ])
            // Utilisation de CKEditor à la place du textarea
            ->add('fullDescription', CKEditorType::class, [
                'label' => 'Description complète'
            ])
            ->add('duration', TextType::class, [
                'label' => 'Durée'
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prix'
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image du cours',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false
            ])
            ->add('schedule', TextType::class, [
                'label' => 'Horaire'
            ])
            ->add('programFile', VichImageType::class, [
                'label' => 'Sélectionnez le programme',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'placeholder' => 'Sélectionnez une catégorie',
                'class' => 'App:CourseCategory',
                // trier les catégories
                'query_builder' => function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name'
            ])
            ->add('level', EntityType::class, [
                'label' => 'Niveau',
                'placeholder' => 'Sélectionnez une catégorie',
                'class' => 'App:CourseLevel',
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
