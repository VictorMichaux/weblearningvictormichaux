<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\Inscription;
use App\Repository\CourseRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class InscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'label' => 'Utilisateur',
                'placeholder' => 'Sélectionnez un utilisateur',
                'class' => 'App:User',
                'query_builder' => function (UserRepository $repository) {
                    return $repository->createQueryBuilder('u')->orderBy('u.firstName', 'ASC');
                },
                'choice_label' => function($user) {
                    return $user->getFirstName().' '.$user->getLastName();
                },
                'choice_value' => 'id'
            ])
            ->add('course', EntityType::class, [
                'label' => 'Cours',
                'placeholder' => 'Sélectionnez un cours',
                'class' => 'App:Course',
                'query_builder' => function (CourseRepository $repository) {
                    return $repository->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Inscription::class,
        ]);
    }
}
