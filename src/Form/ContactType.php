<?php

namespace App\Form;

use App\Entity\Contact;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Votre nom'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Votre adresse e-mail'
            ])
            ->add('receiver', EntityType::class, [
                'label' => 'À qui s\'adresse ce mail ?',
                'placeholder' => 'Sélectionnez un destinataire',
                'class' => 'App:User',
                'query_builder' => function (UserRepository $repository) {
                    return $repository->createQueryBuilder('u')->where("u.roles LIKE '%ADMIN%'")->orderBy('u.lastName', 'ASC');
                },
                'choice_label' => function($user) {
                    return $user->getFirstName().' '.$user->getLastName();
                },
                'choice_value' => 'email'
            ])
            ->add('subject', TextType::class, [
                'label' => 'L\'objet de votre e-mail'
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Votre message'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
