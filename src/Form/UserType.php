<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Votre adresse e-mail'
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Votre nom'
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Votre photo',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false
            ])
            ->add('newsLetter', CheckboxType::class, [
                'label' => 'Inscription à la Newsletter',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
