<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, [
                'label' => 'Votre mot de passe'
            ])
            ->add('oldPasswordVerification', PasswordType::class, [
                'label' => 'Répétez votre mot de passe'
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Votre nouveau mot de passe'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "allow_extra_fields" => true
        ]);
    }
}
