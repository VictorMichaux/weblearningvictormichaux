<?php

namespace App\Security\Service;

use App\Entity\Contact;
use App\Entity\News;
use App\Entity\User;
use App\Repository\UserRepository;
use Twig\Environment;

class ContactService
{
    private $mailer;
    private $renderer;

    // La classe Environment permet d'utiliser Twig pour l'envoi de mail
    public function __construct(\Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function sendMail(Contact $contact)
    {
        $message = (new \Swift_Message())
            ->setFrom($contact->getEmail())
            ->setTo(str_replace("\xE2\x80\x8B", "", $contact->getReceiver()))
            ->setReplyTo($contact->getEmail())
            ->setSubject($contact->getSubject())
            ->setBody($this->renderer->render('contact/emailbasic.html.twig', ['contact' => $contact]), 'text/html');
        $this->mailer->send($message);
    }

    public function sendNewsLetter(News $new, User $user)
    {
        {
            $message = (new \Swift_Message())
                ->setFrom('contact@weblearning.be')
                ->setTo(str_replace("\xE2\x80\x8B", "", $user->getEmail()))
                ->setReplyTo('contact@weblearning.be')
                ->setSubject('[Weblearning] Nouvelle actualité: '.$new->getName())
                ->setBody($this->renderer->render('newsletter/newsletter.html.twig', ['news' => $new, 'user' => $user]), 'text/html');
            $this->mailer->send($message);
        }
    }
}