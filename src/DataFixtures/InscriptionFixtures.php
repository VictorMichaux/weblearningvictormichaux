<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\Inscription;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class InscriptionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $users = $manager->getRepository(User::class)->findAll();
        $courses = $manager->getRepository(Course::class)->findAll();

        for ($i = 1; $i <= 250; $i++) {
            $inscription = new Inscription();
            $inscription->setCreatedAt($faker->dateTimeBetween('-2 years', 'now'));
            $inscription->setUser($users[$faker->numberBetween(0, count($users) - 1)]);
            $inscription->setCourse($courses[$faker->numberBetween(0, count($courses) - 1)]);
            $manager->persist($inscription);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CourseFixtures::class
        ];
    }
}