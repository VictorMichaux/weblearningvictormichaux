<?php

namespace App\DataFixtures;

use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $genders = ['male', 'female'];
    private $roles = [['ROLE_USER'], ['ROLE_ADMIN'], ['ROLE_SUPER_ADMIN']];
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $slug = new Slugify();
        for($i = 1; $i <= 50; $i++) {
            $user = new User();
            $gender = $faker->randomElement(($this->genders));
            $user->setFirstName($faker->firstName($gender));
            $user->setLastName($faker->lastName);
            $user->setEmail($slug->slugify($user->getFirstName()).'.'.$slug->slugify($user->getLastName()).'@gmail.com');
            $gender = $gender == 'male' ? 'm' : 'f';
            $user->setImage('0'.($i + 10).$gender.'.jpg');
            $user->setPassword($this->encoder->encodePassword($user, 'password'));
            $user->setCreatedAt($faker->dateTimeBetween('-6 months', 'now'));
            $user->setUpdatedAt($faker->dateTimeBetween('now'));
            $user->setLastLogAt($faker->dateTimeBetween('now'));
            $user->setIsDisabled($faker->boolean(10));
            $user->setIsVerified(true);
            $user->setRoles($faker->randomElement($this->roles));
            $user->setWarningMessages(0);
            $user->setNewsletter($faker->boolean(50));
            //$user->setRoles(['ROLE_USER']);
            $manager->persist($user);
        }
        /* John Doe role USER */
        $user = new User();
        $user->setFirstName('John');
        $user->setLastName('Doe');
        $user->setEmail('john.doe@gmail.com');
        $user->setImage('075m.jpg');
        $user->setPassword($this->encoder->encodePassword($user, 'password'));
        $user->setCreatedAt($faker->dateTimeBetween('-6 months', 'now'));
        $user->setUpdatedAt($faker->dateTimeBetween('now'));
        $user->setLastLogAt($faker->dateTimeBetween('now'));
        $user->setIsDisabled('0');
        $user->setIsVerified(true);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setWarningMessages(0);
        $user->setNewsletter(true);
        $manager->persist($user);

        /* Victor Michaux role ADMIN */
        $user = new User();
        $user->setFirstName('Victor');
        $user->setLastName('Michaux');
        $user->setEmail('victor.michaux@gmail.com');
        $user->setImage('default.jpg');
        $user->setPassword($this->encoder->encodePassword($user, 'password'));
        $user->setCreatedAt($faker->dateTimeBetween('-6 months', 'now'));
        $user->setUpdatedAt($faker->dateTimeBetween('now'));
        $user->setLastLogAt($faker->dateTimeBetween('now'));
        $user->setIsDisabled('0');
        $user->setIsVerified(true);
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $user->setWarningMessages(0);
        $user->setNewsletter(true);
        $manager->persist($user);

        $manager->flush();
    }
}
