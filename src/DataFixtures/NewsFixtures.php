<?php

namespace App\DataFixtures;

use App\Entity\News;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class NewsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $slugify = new Slugify();
        for($i = 1; $i <= 25; $i++) {
            $news = new News();
            $news->setName($faker->sentence(3));
            $news->setCreatedAt($faker->dateTimeThisYear('now'));
            $news->setContent($faker->paragraph(3));
            $news->setSlug($slugify->slugify($news->getName()));
            $news->setImage('0'.$i.'.png');
            $manager->persist($news);
        }
        $manager->flush();
    }
}
