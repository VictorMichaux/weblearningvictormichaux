<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\AdminUserType;
use App\Repository\UserRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUserController extends AbstractController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @Route("/admin/user", name="admin_user")
     * @param UserRepository $repository
     * @return Response
     */
    public function users(UserRepository $repository): Response
    {
        return $this->render('admin/user/admin-user.html.twig',
            [
                'users' => $repository->findAll()
            ]);
    }

    /**
     * @Route("/admin/user/knp_paginator", name="admin_user_knp_paginator")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param EntityManagerInterface $manager
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function listAction(EntityManagerInterface $manager, PaginatorInterface $paginator, Request $request)
    {
        $dql   = "SELECT u FROM App:User u";
        $query = $manager->createQuery($dql);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        // parameters to template
        return $this->render('admin/user/admin-user-pagination.html.twig', ['pagination' => $pagination]);
    }

    /**
     * @Route("admin/disableUser/{id}", name="admin_user_disable")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function disableUser(User $user, EntityManagerInterface $manager): Response
    {
        $user->setIsDisabled(!$user->getIsDisabled());
        $manager->flush();
        if ($user->getIsDisabled() == true){
            $this->addFlash(
                'success',
                'L\'utilisateur(trice) '.$user->getFirstName().' '.$user->getLastName().' a bien été désactivé(e)
                ');
        }
        else {
            $this->addFlash(
                'success',
                'L\'utilisateur(trice) '.$user->getFirstName().' '.$user->getLastName().' a bien été activé(e)
                ');
        }

        return $this->redirectToRoute('admin_user');
    }

    /**
     * @Route("/admin/newUser", name="admin_user_new")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     * @throws Exception
     */
    public function newUser(EntityManagerInterface $manager, Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $user->setCreatedAt($now);
            $user->setUpdatedAt($now);
            $user->setLastLogAt($now);
            $user->setImage('default.jpg');
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $user->setWarningMessages(0);
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'utilisateur '.$user->getFirstName().' '.$user->getLastName().' a bien été ajouté
                ');
            return $this->redirectToRoute("admin_user");
        }
        return $this->render("admin/user/new-user.html.twig", [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/admin/promote/{id}/{role}", name="admin_user_promote")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param User $user
     * @param EntityManagerInterface $manager
     * @param $role
     * @return Response
     */
    public function promoteUser(User $user, EntityManagerInterface $manager, $role): Response
    {
        $user->setRoles([$role]);
        $manager->flush();
        if ($role == "ROLE_USER") {
            $this->addFlash(
                'success',
                'L\'utilisateur '.$user->getFirstName().' '.$user->getLastName().' est maintenant un Utilisateur'
            );
        }
        elseif ($role == "ROLE_ADMIN") {
            $this->addFlash(
                'success',
                'L\'utilisateur '.$user->getFirstName().' '.$user->getLastName().' est maintenant un Administrateur'
            );
        }
        else {
            $this->addFlash(
                'success',
                'L\'utilisateur '.$user->getFirstName().' '.$user->getLastName().' est maintenant un Super Administrateur'
            );
        }
            return $this->redirectToRoute('admin_user');
    }
}