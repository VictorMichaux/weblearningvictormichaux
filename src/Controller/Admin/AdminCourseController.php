<?php

namespace App\Controller\Admin;


use App\Entity\Course;
use App\Form\CourseType;
use App\Repository\CourseRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCourseController extends AbstractController
{
    /**
     * @Route("/admin/course", name="admin_course")
     * @IsGranted("ROLE_ADMIN")
     * @param CourseRepository $repository
     * @return Response
     */
    public function courses(CourseRepository $repository): Response
    {
        return $this->render('admin/course/admin-course.html.twig',
            [
                'courses' => $repository->findAll()
            ]);
    }

    /**
     * @Route("admin/viewcourse/{id}", name="admin_course_view")
     * @IsGranted("ROLE_ADMIN")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function viewCourse(Course $course, EntityManagerInterface $manager): Response
    {
        $course->setIsPublished(!$course->getIsPublished());
        $manager->flush();
        $this->addFlash(
            'success',
            'Le cours '.$course->getName().' a bien été affiché/masqué'
        );
        return $this->redirectToRoute('admin_course');
    }

    /**
     * @Route("/admin/delcourse/{id}", name="admin_course_del")
     * @IsGranted("ROLE_ADMIN")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delcourse(Course $course, EntityManagerInterface $manager) : Response
    {
        for ($i=0; $i<count($course->getComments()); $i++)
        {
            if ($course->getComments()[$i]->getWarningMessage())
            {
                $course->getComments()[$i]->getAuthor()->setWarningMessages($course->getComments()[$i]->getAuthor()->getWarningMessages() - 1);
            }
        }
        $manager->remove($course);
        $manager->flush();
        $this->addFlash(
            'success',
            'Le cours '.$course->getName().' a bien été supprimé'
        );
        return $this->redirectToRoute("admin_course");
    }

    /**
     * @Route("/admin/newcourse", name="admin_course_new")
     * @IsGranted("ROLE_ADMIN")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function newCourse(EntityManagerInterface $manager, Request $request): Response
    {
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $course->setSlug($slugify->slugify($course->getName()));
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $course->setCreatedAt($now);
            $course->setIsPublished(true);
            $manager->persist($course);
            $manager->flush();
            $this->addFlash(
                'success',
                'Le cours '.$course->getName().' a bien été ajouté
                ');
            return $this->redirectToRoute("admin_course");
        }
        return $this->render("admin/course/new-course.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/editcourse/{id}", name="admin_course_edit")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function editCourse(Course $course, EntityManagerInterface $manager, Request $request): Response
    {
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $course->setSlug($slugify->slugify($course->getName()));
            $manager->persist($course);
            $manager->flush();
            $this->addFlash(
                'success',
                'Le cours '.$course->getName().' a bien été modifié
                ');
            return $this->redirectToRoute("admin_course");
        }
        return $this->render("admin/course/edit-course.html.twig", [
            'form' => $form->createView()
        ]);
    }
}