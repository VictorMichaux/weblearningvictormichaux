<?php

namespace App\Controller\Admin;


use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use App\Repository\UserRepository;
use App\Security\Service\ContactService;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminNewsController extends AbstractController
{
    /**
     * @Route("/admin/news", name="admin_news")
     * @param NewsRepository $repository
     * @return Response
     */
    public function news(NewsRepository $repository): Response
    {
        return $this->render('admin/news/admin-news.html.twig',
            [
                'news' => $repository->findBy(
                    [],
                    ['createdAt'=>'DESC']
                )
            ]);
    }

    /**
     * @Route("/admin/delNews/{id}", name="admin_news_del")
     * @IsGranted("ROLE_ADMIN")
     * @param News $new
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delNews(News $new, EntityManagerInterface $manager) : Response
    {
        $manager->remove($new);
        $manager->flush();
        $this->addFlash(
            'success',
            'L\'actualité '.$new->getName().' a bien été supprimée'
        );
        return $this->redirectToRoute("admin_news");
    }

    /**
     * @Route("/admin/newNews", name="admin_news_new")
     * @IsGranted("ROLE_ADMIN")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param ContactService $contactService
     * @param UserRepository $repository
     * @return Response
     * @throws Exception
     */
    public function newNews(EntityManagerInterface $manager, Request $request, ContactService $contactService, UserRepository $repository): Response
    {
        $new = new News();
        $form = $this->createForm(NewsType::class, $new);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $new->setSlug($slugify->slugify($new->getName()));
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $new->setCreatedAt($now);
            $manager->persist($new);
            $manager->flush();
            $user = $repository->findBy(['newsletter' => true]);
            for ($i=1; $i<count($user); $i++)
            {
                $contactService->sendNewsLetter($new, $user[$i]);
            }
            $this->addFlash(
                'success',
                'L\'actualité '.$new->getName().' a bien été ajoutée et une newsletter a été envoyée'
            );
            return $this->redirectToRoute("admin_news");
        }
        return $this->render("admin/news/new-new.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/editNews/{id}", name="admin_news_edit")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param News $new
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    public function editNews(News $new, EntityManagerInterface $manager, Request $request): Response
    {
        $form = $this->createForm(NewsType::class, $new);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $new->setSlug($slugify->slugify($new->getName()));
            $manager->persist($new);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'actualité '.$new->getName().' a bien été modifiée
                ');
            return $this->redirectToRoute("admin_news");
        }
        return $this->render("admin/news/edit-news.html.twig", [
            'form' => $form->createView()
        ]);
    }
}