<?php

namespace App\Controller\Admin;


use App\Entity\Inscription;
use App\Form\InscriptionType;
use App\Repository\InscriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminInscriptionController extends AbstractController
{
    /**
     * @Route("/admin/inscription", name="admin_inscription")
     * @param InscriptionRepository $repository
     * @return Response
     */
    public function inscriptions(InscriptionRepository $repository): Response
    {
        return $this->render('admin/inscription/admin-inscription.html.twig',
            [
                'inscriptions' => $repository->findAll()
            ]);
    }

    /**
     * @Route("/admin/delInscription/{id}", name="admin_inscription_del")
     * @IsGranted("ROLE_ADMIN")
     * @param Inscription $inscription
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delInscription(Inscription $inscription, EntityManagerInterface $manager) : Response
    {
        $manager->remove($inscription);
        $manager->flush();
        $this->addFlash(
            'success',
            'L\'inscription de '.$inscription->getUser()->getFirstName().' '.$inscription->getUser()->getLastName().' au cours '.$inscription->getCourse()->getName().' a bien été supprimée'
        );
        return $this->redirectToRoute("admin_inscription");
    }

    /**
     * @Route("/admin/newInscription", name="admin_inscription_new")
     * @IsGranted("ROLE_ADMIN")
     * @param InscriptionRepository $repository
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function newInscription(InscriptionRepository $repository, EntityManagerInterface $manager, Request $request): Response
    {
        $inscription = new Inscription();
        $inscriptions = $repository->findAll();
        $form = $this->createForm(InscriptionType::class, $inscription);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            for ($i=0; $i<count($inscriptions); $i++)
            {
                if ($inscriptions[$i]->getUser() == $inscription->getUser() && $inscriptions[$i]->getCourse() == $inscription->getCourse())
                {
                    $this->addFlash(
                        'warning',
                        $inscription->getUser()->getFirstName().' '.$inscription->getUser()->getLastName().' est déjà inscrit au cours '.$inscription->getCourse()->getName()
                    );
                    return $this->redirectToRoute('admin_inscription_new');
                }
            }
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $inscription->setCreatedAt($now);
            $manager->persist($inscription);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'inscription de '.$inscription->getUser()->getFirstName().' '.$inscription->getUser()->getLastName().' au cours '.$inscription->getCourse()->getName().' a bien été ajoutée'
                );
            return $this->redirectToRoute("admin_inscription");
        }
        return $this->render("admin/inscription/new-inscription.html.twig", [
            'form' => $form->createView()
        ]);
    }
}