<?php

namespace App\Controller\Admin;


use App\Entity\Comment;
use App\Form\CommentMessageType;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCommentController extends AbstractController
{
    /**
     * @Route("/admin/comments", name="admin_comment")
     * @param CommentRepository $repository
     * @return Response
     */
    public function comments(CommentRepository $repository): Response
    {
        return $this->render('admin/comment/admin-comment.html.twig',
            [
                'comments' => $repository->findBy(
                    ['isDisabled' => false],
                    ['createdAt' => 'ASC']
                )
            ]);
    }

    /**
     * @Route("admin/disableComment/{id}", name="admin_comment_disable")
     * @IsGranted("ROLE_ADMIN")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function disableComment(Comment $comment, EntityManagerInterface $manager): Response
    {
        $comment->setIsDisabled(true);
        $manager->flush();
        return $this->redirectToRoute('admin_comment');
    }

    /**
     * @Route("admin/publishComment/{id}", name="admin_comment_publish")
     * @IsGranted("ROLE_ADMIN")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function publishComment(Comment $comment, EntityManagerInterface $manager): Response
    {
        $comment->setIsDisabled(false);
        if ($comment->getWarningMessage())
        {
            $comment->setWarningMessage(null);
            $comment->getAuthor()->setWarningMessages($comment->getAuthor()->getWarningMessages()-1);
        }
        $manager->flush();
        return $this->redirectToRoute('admin_comment_disabled');
    }

    /**
     * @Route("admin/disabledComments", name="admin_comment_disabled")
     * @IsGranted("ROLE_ADMIN")
     * @param CommentRepository $repository
     * @return Response
     */
    public function CommentContactAuthor(CommentRepository $repository): Response
    {
        return $this->render("admin/comment/admin-comment-contact-author.html.twig", [
            'comments' => $repository->findBy(['isDisabled' => 1])
        ]);
    }

    /**
     * @Route("/admin/commentMessage/{id}", name="admin_comment_message")
     * @IsGranted("ROLE_ADMIN")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    public function CommentContactAuthorMessage(Comment $comment, EntityManagerInterface $manager, Request $request) : Response
    {
        $form = $this->createForm(CommentMessageType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if ($comment->getWarningMessage() == null)
            {
                $comment->getAuthor()->setWarningMessages($comment->getAuthor()->getWarningMessages() + 1);
            }
            $comment->setWarningMessage($form->getData()['message']);
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash(
                'success',
                'Votre message sur le commentaire de '.$comment->getAuthor()->getFirstName().' '.$comment->getAuthor()->getLastName().' a bien été enregistré
                ');
            return $this->redirectToRoute("admin_comment_disabled");
        }
        return $this->render("admin/comment/admin-comment-message.html.twig", [
            'form' => $form->createView()
        ]);
    }
}