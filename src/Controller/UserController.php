<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Course;
use App\Form\PasswordUpdateType;
use App\Form\UserType;
use App\Repository\CommentRepository;
use App\Repository\InscriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/myAccount", name="myAccount")
     * @return Response
     */
    public function myAccount()
    {
        return $this->render("user/account.html.twig");
    }


    /**
     * @Route("/AccountUpdate", name="account_update")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function accountUpdate(EntityManagerInterface $manager, Request $request)
    {
        $user=$this->getUser();
        $form=$this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $user->setUpdatedAt($now);
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                'Vos informations ont bien été modifiées'
            );
            return $this->redirectToRoute("myAccount");
        }
        return $this->render("user/account-update.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/PasswordUpdate", name="password_update")
     * @param EntityManagerInterface $manager
     * @param UserPasswordEncoderInterface $encoder
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function passwordUpdate(EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder, Request $request) : Response
    {
        $user=$this->getUser();
        $form=$this->createForm(PasswordUpdateType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()['oldPassword'] == $form->getData()['oldPasswordVerification'] && password_verify($form->getData()['oldPassword'], $user->getPassword())) {
                $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
                $user->setUpdatedAt($now);
                $user->setPassword($encoder->encodePassword($user, $form->getData()['password']));
                $manager->persist($user);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Votre mot de passe a bien été modifié'
                );
                return $this->redirectToRoute("password_update");
            } elseif ($form->getData()['oldPassword'] != $form->getData()['oldPasswordVerification']) {
                $this->addFlash(
                    'danger',
                    'Vous avez entré deux mots de passe différents. Votre mot de passe n\'a pas été modifié'
                );
            } elseif(!password_verify($form->getData()['oldPassword'], $user->getPassword())) {
                $this->addFlash(
                    'danger',
                    'Vous avez entré un mauvais mot de passe. Votre mot de passe n\'a pas été modifié'
                );
            }
        }
        return $this->render("user/password-update.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/myComments", name="myComments")
     * @param CommentRepository $repository
     * @return Response
     */
    public function myComments(CommentRepository $repository)
    {
        $comments = $repository->findBy(
            ['author' => $this->getUser()],
            [
                'isDisabled' => 'DESC',
                'warningMessage' => 'DESC',
                'createdAt' => 'ASC'
            ]
        );
        return $this->render("user/comments.html.twig", [
                'comments' => $comments]
        );
    }

    /**
     * @Route("/delCommentUser/{slug}", name="delCommentUser")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delCommentUser(Course $course, EntityManagerInterface $manager) : Response
    {
        for($i=0; $i<count($course->getComments()); $i++)
        {
            if ($course->getComments()[$i]->getAuthor() == $this->getUser())
            {
                $comment = $course->getComments()[$i];
                if ($comment->getWarningMessage())
                {
                    $comment->setWarningMessage(null);
                    $comment->getAuthor()->setWarningMessages($comment->getAuthor()->getWarningMessages()-1);
                }
                $manager->remove($comment);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Votre commentaire sur le cours '.$course->getName().' a bien été supprimé'
                );
            }
        }
        return $this->redirectToRoute('myComments');
    }

    /**
     * @Route("/myInscriptions", name="myInscriptions")
     * @param InscriptionRepository $repository
     * @return Response
     */
    public function myInscriptions(InscriptionRepository $repository)
    {
        $inscriptions = $repository->findBy(
            ['user' => $this->getUser()],
            ['course' => 'ASC']
        );
        return $this->render("user/inscriptions.html.twig", [
                'inscriptions' => $inscriptions]
        );
    }

    /**
     * @Route("/delInscriptionUser/{slug}", name="delInscriptionUser")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delInscriptionUser(Course $course, EntityManagerInterface $manager) : Response
    {
        for($i=0; $i<count($course->getInscription()); $i++)
        {
            if ($course->getInscription()[$i]->getUser() == $this->getUser())
            {
                $inscription = $course->getInscription()[$i];
                $manager->remove($inscription);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Vous avez bien été désinscrit(e) du cours '.$course->getName()
                );
            }
        }
        return $this->redirectToRoute('myInscriptions');
    }
}
