<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Course;
use App\Entity\Inscription;
use App\Form\CommentType;
use App\Repository\CourseCategoryRepository;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CourseController
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/courses", name="courses")
     * @param CourseCategoryRepository $repoCategory
     * @param CourseRepository $repoCourse
     * @return Response
     */
    public function courses(CourseCategoryRepository $repoCategory, CourseRepository $repoCourse): Response
    {
        $courses = $repoCourse->findBy(
            ['isPublished' => true],
            ['name' => 'ASC']
        );
        $categories = $repoCategory->findBy(
            [],
            ['name' => 'ASC']
        );
        return $this->render('course/courses.html.twig', [
            'courses' => $courses,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/course/{slug}", name="course")
     * @param Course $course
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    // Uitilisation du composant ParamConverter
    public function course(Course $course, Request $request, EntityManagerInterface $manager): Response
    {
        //Formulaire de postComment
        $comment = new Comment();
        for($i=0; $i<count($course->getComments()); $i++)
        {
            if ($course->getComments()[$i]->getAuthor() == $this->getUser())
            {
                $comment = $course->getComments()[$i];
            }
        }
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setAuthor($this->getUser());
            $comment->setCourse($course);
            $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
            $comment->setCreatedAt($now);
            $comment->setIsDisabled(true);
            if ($comment->getWarningMessage())
            {
                $comment->setWarningMessage(null);
                $comment->getAuthor()->setWarningMessages($comment->getAuthor()->getWarningMessages()-1);
            }
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash(
                'success',
                'Votre commentaire sur le cours '.$course->getName().' a bien été ajouté ou modifié et est en attente de validation par un administrateur'
            );
            return $this->redirect($request->getUri());
        }
        return $this->render('course/detail.html.twig', [
            'course' => $course,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delCommentCourse/{slug}", name="delCommentCourse")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delCommentCourse(Course $course, EntityManagerInterface $manager) : Response
    {
        for($i=0; $i<count($course->getComments()); $i++)
        {
            if ($course->getComments()[$i]->getAuthor() == $this->getUser())
            {
                $comment = $course->getComments()[$i];
                if ($comment->getWarningMessage())
                {
                    $comment->setWarningMessage(null);
                    $comment->getAuthor()->setWarningMessages($comment->getAuthor()->getWarningMessages()-1);
                }
                $manager->remove($comment);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Votre commentaire sur le cours '.$course->getName().' a bien été supprimé'
                );
            }
        }
        return $this->redirectToRoute('course', ['slug' => $course->getSlug()]);
    }

    /**
     * @Route("/addInscriptionCourse/{slug}", name="addInscriptionCourse")
     * @param Course $course
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws Exception
     */
    public function inscription(Course $course, Request $request, EntityManagerInterface $manager): Response
    {
        $inscription = new Inscription();
        $inscription->setUser($this->getUser());
        $inscription->setCourse($course);
        $now = new \DateTime('now', new \DateTimeZone('Europe/Brussels'));
        $inscription->setCreatedAt($now);
        $manager->persist($inscription);
        $manager->flush();
        $this->addFlash(
            'success',
            'Vous avez bien été inscrit(e) au cours'.$course->getName()
        );
        return $this->redirectToRoute('course', ['slug' => $course->getSlug()]);
    }

    /**
     * @Route("/delInscriptionCourse/{slug}", name="delInscriptionCourse")
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delInscriptionCourse(Course $course, EntityManagerInterface $manager) : Response
    {
        for($i=0; $i<count($course->getInscription()); $i++)
        {
            if ($course->getInscription()[$i]->getUser() == $this->getUser())
            {
                $inscription = $course->getInscription()[$i];
                $manager->remove($inscription);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Vous avez bien été désinscrit(e) du cours '.$course->getName()
                );
            }
        }
        return $this->redirectToRoute('course', ['slug' => $course->getSlug()]);
    }
}
