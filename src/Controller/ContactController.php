<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Security\Service\ContactService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     * @param Request $request
     * @param ContactService $contactService
     * @return Response
     */
    public function contact(Request $request, ContactService $contactService): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $secretKey = '6LfyBR8aAAAAAIfz9jTuTWEsgl8tf3--dTskR5c9';
            $responseKey = $_POST['g-recaptcha-response'];
            $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey";
            $response = json_decode(file_get_contents($url));
            if ($response->success) {
                // Gestion du mail avec la classe Service\Contact
                $contactService->sendMail($contact);
                $this->addFlash(
                    'success',
                    'Votre messsage a bien été envoyé'
                );
                return $this->redirectToRoute('home');
            }
            else
            {
                $this->addFlash('warning', 'Vous n\'avez pas rempli le captcha');
            }
        }
        return $this->render('contact/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
