Instructions
=
Mettre à jour Composer
`composer update`

Créer la base de données 

`php bin/console doctrine:database:create`

Migrer les tables

`php bin/console doctrine:migrations:migrate`

Loader les fixtures

`php bin/console doctrine:fixtures:load`

Installer Webpack Encore

`npm install`

Démarrer le serveur Webpack

`npm run dev`

Démarrer le serveur Symfony

`symfony server:start`

Pour s'identifier en tant que Super Admin,
>Adresse e-mail: victor.michaux@gmail.com

>Mot de passe: password
