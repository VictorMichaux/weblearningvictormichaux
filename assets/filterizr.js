import 'jquery/dist/jquery.min.js';

import $ from 'jquery';

import Filterizr from 'filterizr';
// This will extend the $.fn prototype with Filterizr
Filterizr.installAsJQueryPlugin($);
$('.filter-container').filterizr(); // or any other Filterizr API call